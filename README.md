<br />
<div align="center">
<img src="https://gitlab.com/MatzE_/spread-lamps-evenly/-/raw/master/thumbnail.png" alt="Logo" width="80" height="80">
<h3 align="center">Spread Lamps Evenly</h3>
<p align="center">Small project to dots lamps evenly on a rectangle. Especially helpful in electrical room planning which it was programmed to support.
<br />
<a href="https://gitlab.com/MatzE_/spread-lamps-evenly/-/issues/new">Report Bug</a> · <a href="https://gitlab.com/MatzE_/spread-lamps-evenly/-/issues/new">Request Feature</a>
</p>
</div>

