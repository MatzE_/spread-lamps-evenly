
// class Line {
//     constructor(x1, y1, x2, y2, color) {
//         this.line = new createjs.Shape();
//         line.graphics.setStrokeStyle(2).beginStroke(color);
//         line.graphics.moveTo(x1, y1);
//         line.graphics.lineTo(x2, y2);
//     }
//     drawables() {
//         return this.line;
//     }
// }

class Drawable {
    constructor(func, ...args) {
        this.func = func;
        this.args = args;
    }
    update(...args){
        this.args = args;
    }
    draw(canvas) {
        this.func.bind(canvas)(...this.args);
    }
}

class Room extends Drawable {
    constructor(canvas) {
        super(canvas.addRoom);
        this.canvas = canvas;
        canvas.add('room', this);
    }
    setDimensions(length, width) {
        this.length = Number(length);
        this.width = Number(width);
        this.update(this.length, this.width);
    }
}

class Line extends Drawable {
    constructor(canvas,name, x, y) {
        super(canvas.addLine);
        this.canvas = canvas;
        this.args = [x,y]
        canvas.add(name,this);
    }
    draw(canvas) {
        this.func.bind(canvas)(...this.args);
    }
}

class Lamp extends Drawable {
    constructor(canvas, x, y) {
        super(canvas.addLight);
        this.canvas = canvas;
        this.setXY(x,y);
    }
    setXY(x, y) {
        this.x = Number(x);
        this.y = Number(y);
        this.update(this.x, this.y);
    }
    draw() {
        super.draw(this.canvas);
    }
}

class Canvas {
    constructor(el, width, height, drawables = {}) {
        this.el = el;
        this.width = width;
        this.height = height;
        this.stage = new createjs.Stage(this.el);
        this.drawables = drawables;
        this.lamps = null;
        
        this._mInPx = 50;
        this.backgroundOffset = 10; //px
        
        this.el.width = this.width;
        this.el.height = this.height;
        
        this.drawBackground();
    }
    set mInPx(px) {
        this._mInPx = px;
        if (this._mInPx <= 5) {
            this._mInPx = 5;
        }
    }
    get mInPx() {
        return this._mInPx;
    }
    drawBackground() {
        // background blue
        let background = new createjs.Shape();
        background.graphics.beginFill(BG_BLUE).drawRect(0, 0, this.width, this.height);
        background.x = 0;
        background.y = 0;
        this.stage.addChild(background);

        // lines
        for (let i = this.backgroundOffset; i < Math.max(this.height, this.width); i += this.mInPx) {
            this.addLine(0, i, this.width, i);
            this.addLine(i, 0, i, this.height);
        }

        this.stage.update();
    }
    addLine(x1, y1, x2, y2, color = BG_LINE_BLUE) {
        // Get another 'line shape' object (really just a shape we'll use to draw a line
        const line = new createjs.Shape();
                
        this.stage.addChild(line);
        // Tell EaselJs that we want a thin like (1px) with a color
        line.graphics.setStrokeStyle(1).beginStroke(color);

        // Point EaselJS to the place where we want to start drawing
        line.graphics.moveTo(x1,y1);

        // Tell EaselJS to draw a line to a different point
        line.graphics.lineTo(x2, y2);
        // Add the line shape to the canvas
    }
    addCircle(x, y, r, color = STROKE_WHITE){
        const circle = new createjs.Shape();
        canvas.stage.addChild(circle);
        circle.graphics
            .setStrokeStyle(1)
            .beginStroke(color)
            .drawCircle(x,y,r);
    }
    m2px(m) {
        return this.mInPx * m;
    }
    px2m(px) {
        return px / this.mInPx;
    }
    addRect(x1, y1, x2, y2, color = STROKE_WHITE) {
        let line = new createjs.Shape();

        // Add this line shape to the canvas
        this.stage.addChild(line);

        //                           v stroke width in Pixel of Canvas?
        line.graphics.setStrokeStyle(1).beginStroke(color);

        // Tell EaselJS where to go to start drawing the line
        line.graphics.moveTo(x1, y1);

        line.graphics.lineTo(x1, y2);
        line.graphics.lineTo(x2, y2);
        line.graphics.lineTo(x2, y1);
        line.graphics.lineTo(x1, y1);

        // Stop drawing this line
        line.graphics.endStroke();
    }
    addRectCenter(height, width) {
        const centerX = this.width / 2;
        const centerY = this.height / 2;

        const x1 = centerX - width / 2;
        const y1 = centerY - height / 2;

        const x2 = centerX + width / 2;
        const y2 = centerY + height / 2;

        this.addRect(x1, y1, x2, y2);
    }
    /**
     * Adds a Rectangle in the center as room
     * @param {Number} length in meter
     * @param {Number} width in meter
     */
    addRoom(length, width) {
        this.addRectCenter(this.m2px(length),this.m2px(width));
        this.stage.update();
    }
    /**
     * get Pixel coordinate of rooms top left corner
     * @returns {array} [x, y] coords in px
     */
    getRoomTopLeft() {
        const room = this.drawables['room'];
        const width = this.m2px(room.args[1]);
        const height = this.m2px(room.args[0]);

        const centerX = this.width / 2;
        const centerY = this.height / 2;
        const x = centerX - width / 2;
        const y = centerY - height / 2;

        return [x, y]
    }
    /**
     * Adds a light symbol in the room top left = 0,0 meter
     * @param {Number} meter_x in meter from room corner
     * @param {Number} meter_y in meter from room corner
     */
    addLight(meter_x, meter_y) {
        const [roomCornerTLX, roomCornerTLY] = this.getRoomTopLeft();
        const x = roomCornerTLX + this.m2px(meter_x);
        const y = roomCornerTLY + this.m2px(meter_y);
        
        // circle
        const radius_px = this.m2px(LIGHT_RADIUS);
        this.addCircle(x, y, radius_px);
        // cross
        //                         v----v--- sin(45°)
        const offset = radius_px * 0.7071;
        this.addLine(x - offset, y - offset, x + offset, y + offset, STROKE_WHITE);
        this.addLine(x - offset, y + offset, x + offset, y - offset, STROKE_WHITE);

    }

    add(name, drawable) {
        this.drawables[name] = drawable;
        this.draw();
    }
    /**
     * Draws all drawables in this.drawables
     * and this.lamps.drawables
     */
    draw(additionalDrawables = []) {
        this.clear();
        // drawables of canvas
        for (let name of Object.keys(this.drawables)) {
            const drawable = this.drawables[name];
            drawable.draw(this);
        }
        // drawables of lamps
        if (this.lamps) {
            for (let drawable of this.lamps.drawables) {
                drawable.draw(this);
            }
        }

        this.stage.update();
    }
    clear() {
        this.stage.clear();
        this.drawBackground();
    }
    zoom(diff) {
        this.mInPx += diff * this.mInPx / 100;
        this.draw();
    }
}


class Lamps {
    /**
     * @param {Room} room to fill with lamps
     * @param {Object} domElements DOM elements to interact with lamps
     *      {
     *          slider: <noUiSliderEl>,
     *          currentPossibility: <span>,
     *          maxPossibilities: <span>,
     *          nextPossibility: <button>,
     *          prevPossibility: <button>
     *          measures: {
     *              dx: <span>,
     *              mx: <span>,
     *              dy: <span>,
     *              my: <span>,
     *          }
     *      }
     */
    constructor(room, domElements) {
        this.sliderEl = domElements.slider.noUiSlider;
        this.currentPossibilityEl = domElements.currentPossibility;
        this.maxPossibilitiesEl = domElements.maxPossibilities;
        this.nextPossibilityEl = domElements.nextPossibility;
        this.prevPossibilityEl = domElements.prevPossibility;
        
        this.measureEls = domElements.measures;

        this.room = room;
        this.canvas = this.room.canvas;
        this.canvas.lamps = this;

        // // list of possible drawable lists
        // this.drawables = [];

        /**
         * e.g.:
         * [
         *  {
         *      length: {lamps: 3, distance: 0.5},
         *      width: {lamps: 2, distance: 2}
         *  }
         * ]
         */
        this.possibilities = []



        this.addEventListeners();
    }
    addEventListeners() {
        // slider listeners set in index.js

        this.nextPossibilityEl.addEventListener('click', evt => {
            this.possibilityDisplayed++;
            canvas.draw();
        });
        this.prevPossibilityEl.addEventListener('click', evt => {
            this.possibilityDisplayed--;
            canvas.draw();
        });

    }

    get possibilityDisplayed() {
        return Number(this.currentPossibilityEl.innerText);
    }
    set possibilityDisplayed(possibility) {
        this.currentPossibilityEl.innerText = possibility;
        this.updateButtonDisabilities();
    }
    get maxPossibilities() {
        return Number(this.maxPossibilitiesEl.innerText);
    }
    set maxPossibilities(n) {
        if (this.possibilityDisplayed == 0) {
            this.possibilityDisplayed = 1;
        }
        this.maxPossibilitiesEl.innerText = n;
        this.updateButtonDisabilities();
    }

    get minDistance() {
        return this.sliderEl.get()[0];
    }
    get maxDistance() {
        return this.sliderEl.get()[1];
    }
    updateButtonDisabilities() {  
        this.prevPossibilityEl.disabled = 
            this.possibilityDisplayed == 1 ||
            this.maxPossibilities == 0;
        this.nextPossibilityEl.disabled = 
            this.possibilityDisplayed == this.maxPossibilities;
    }

    /**
     * e.g.:
     * [
     *  {
     *      length: {lamps: 3, distance: 0.5},
     *      width: {lamps: 2, distance: 2}
     *  }
     * ]
     */
    calcPossibilities() {
        const possibilities = [];

        const widthPossibilities = Lamps.numLamps1d(this.room.width, this.minDistance, this.maxDistance);
        const lengthPossibilities = Lamps.numLamps1d(this.room.length, this.minDistance, this.maxDistance);

        outerLoop:
        for (let widthPoss of widthPossibilities) {
            for (let lengthPoss of lengthPossibilities) {
                possibilities.push({
                    length: lengthPoss,
                    width: widthPoss
                });
                // console.log('going through possibilities: ' + possibilities.length)
                // handle too many possibilities
                if (possibilities.length >= MAX_POSSIBILITIES_TO_CALC) {
                    TOO_MANY_POSSIBILITIES.hidden = false;
                    SLIDER.classList.add('warn');
                    break outerLoop;
                } else {
                    TOO_MANY_POSSIBILITIES.hidden = true;
                    SLIDER.classList.remove('warn');
                }
            }
        }
        // handle nothing possible
        if (possibilities.length == 0) {
            SLIDER.classList.add('error');
            NO_POSSIBILITIES.hidden = false;
        } else {
            SLIDER.classList.remove('error');
            NO_POSSIBILITIES.hidden = true;
        }
        this.maxPossibilities = possibilities.length;
        if (this.maxPossibilities < this.possibilityDisplayed) {
            this.possibilityDisplayed = this.maxPossibilities;
        }
        this.possibilities = possibilities;
    }

    /**
     * calculates the drawables
     * Sideeffect! updates the displayed measures!
     */
    get drawables() {
        // determin which possibility of drawables should be generated:
        // TODO 
        const possibility = this.possibilities[this.possibilityDisplayed - 1]
        // const possibility = {
        //     length: {lamps: 3, distance: 0.5},
        //     width: {lamps: 2, distance: 2}
        // }

        // generate drawables of this possibility
        const drawables = [];
        const distanceX = possibility.width.distance;
        const distanceY = possibility.length.distance;
        // x corosbonds to width
        for (let lampsX = 1; lampsX <= possibility.width.lamps; lampsX++) { 
            const x = (-distanceX/2) + lampsX * distanceX;
            // console.log(`distanceX: ${distanceX}, lampsX: ${lampsX}, x: ${x}`)

            // y corosbonds to length
            for (let lampsY = 1; lampsY <= possibility.length.lamps; lampsY++) { 
                const y = (-distanceY/2) + lampsY * distanceY;

                const lamp = new Lamp(this.canvas, x, y);
                drawables.push(lamp);
            }
        }

        // Update displayed measures
        this.measureEls.dx.innerText = roundXWithYDecimals(distanceX, DECIMALS);
        this.measureEls.mx.innerText = roundXWithYDecimals(distanceX / 2, DECIMALS);
        this.measureEls.dy.innerText = roundXWithYDecimals(distanceY, DECIMALS);
        this.measureEls.my.innerText = roundXWithYDecimals(distanceY / 2, DECIMALS);
        // lampCount
        this.measureEls.lampCount.innerText = drawables.length;

        return drawables;
    }
    /**
     * calculates the possible distributions of lamps in 1d
     * @param lenght of room in m
     * @param minDistance min between lamps in m
     * @param maxDistance max between lamps in m
     * @returns [] if nothing is possible,
     *          [
     *              {lamps: n, distance: d},
     *              {lamps: n2, distance: d2},
     *              ...
     *          ] if n lamps are possible with distance d
     */
    static numLamps1d(length, minDistance, maxDistance) {
        const possibleLampNums = [];

        // test all possiblities
        for(let lampCount = 1; true; lampCount++) {
            
            // distance between lamps would be:
            const distance = length / lampCount;
            // continue / break if we do not have an ok num of lamps
            if (distance > maxDistance) continue;
            if (distance < minDistance) break;
            // found valid lampCount:
            possibleLampNums.push(
                {lamps: lampCount, distance: distance}
            );
        }

        return possibleLampNums;
    }


}