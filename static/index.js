// create canvas
const canvas = new Canvas(
    PLAN_CANVAS,
    document.body.clientWidth - 10, 
    300
);

// CREATE SLIDER

noUiSlider.create(SLIDER, {
    range: {
        min: MIN_DISTANCE,
        max: MAX_DISTANCE
    },
    start: [0.90, 1.20],
    connect: true,
    tooltips: true,
})

// ROOM
// create room
// const room = new Drawable(canvas.addRoom);
const room = new Room(canvas);

const domElements = {
    slider: SLIDER,
    currentPossibility: CURRENT_POSSIBILITY,
    maxPossibilities: MAX_POSSIBILITIES,
    nextPossibility: PREV_POSSIBILITY,
    prevPossibility: NEXT_POSSIBILITY,
    measures: {
        dx: MEASURE_DX,
        mx: MEASURE_MX,
        dy: MEASURE_DY,
        my: MEASURE_MY,
        lampCount: MEASURE_LAMPS
    }
};
const lamps = new Lamps(room, domElements);

// listen to room dimension changes
WIDTH_INPUT.addEventListener('input', roomDimensionChange);
LENGTH_INPUT.addEventListener('input', roomDimensionChange);
function roomDimensionChange() {
    const length = LENGTH_INPUT.value;
    const width = WIDTH_INPUT.value;
    room.setDimensions(length, width);
    lamps.calcPossibilities();
    canvas.draw();
}
roomDimensionChange();

// LISTEN TO ZOOM
ZOOM_IN_BTN.addEventListener('click', changeZoom(10));
ZOOM_OUT_BTN.addEventListener('click', changeZoom(-10));
function changeZoom(diff) {
    return () => canvas.zoom(diff);
}

let shouldUpdate = true;
SLIDER.noUiSlider.on('update', function (values, handle) {
    shouldUpdate = true;
});

setInterval(()=> {
    if (!shouldUpdate) return; 
    lamps.calcPossibilities();
    canvas.draw();
    shouldUpdate = false;
    // console.log("updated");
}, 100);

function roundXWithYDecimals(x, y) {
    const powerOf10 = Math.pow(10, y);
    return Math.round(x * powerOf10) / powerOf10;
}
