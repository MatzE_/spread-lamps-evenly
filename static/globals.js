// GLOBALS / CONSTANTS
// colors
const BG_BLUE = "#3057e1";
const BG_LINE_BLUE = "#4266e4";
const STROKE_WHITE = "#ffffff";

// ui values
const MAX_DISTANCE = 2;
const MIN_DISTANCE = 0.3;
const DECIMALS = 2;

// MISC / ANTI-LAG
const WAIT_ANTI_LAG = 500;
const MAX_POSSIBILITIES_TO_CALC = 100;

// plan values
const LIGHT_RADIUS = 0.1;

// elements
const WIDTH_INPUT = document.getElementById('width');
const LENGTH_INPUT = document.getElementById('length');

const ZOOM_IN_BTN = document.getElementById('zoomin');
const ZOOM_OUT_BTN = document.getElementById('zoomout');

const CURRENT_POSSIBILITY = document.getElementById('possibility');
const MAX_POSSIBILITIES = document.getElementById('possibilities');
const PREV_POSSIBILITY = document.getElementById('nextPossibility');
const NEXT_POSSIBILITY = document.getElementById('prevPossibility');

const TOO_MANY_POSSIBILITIES = document.getElementById('tooManyPossibilities');
const NO_POSSIBILITIES = document.getElementById('noPossibilities');

const SLIDER = document.getElementById('slider');

//// measure elements
const MEASURE_DX = document.getElementById('measure-dx');
const MEASURE_MX = document.getElementById('measure-mx');
const MEASURE_DY = document.getElementById('measure-dy');
const MEASURE_MY = document.getElementById('measure-my');
const MEASURE_LAMPS = document.getElementById('measure-lamps');


// Canvas
const PLAN_CANVAS = document.getElementById('plan');
